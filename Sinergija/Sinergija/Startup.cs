﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Sinergija.Startup))]
namespace Sinergija
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
