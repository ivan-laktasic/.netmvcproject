﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Sinergija.DAL;
using Sinergija.DAL.Repository;
using Sinergija.Model;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using Ninject;

namespace Sinergija.Controllers
{

    public class TransactionsController : Controller
    {
        [Inject]
        public TransactionRepository TransactionRepository { get; set; }
        [Inject]
        public BankAccountRepository BankAccountRepository { get; set; }

        // GET: Transactions
        [Authorize]
      
        public ActionResult Index()
        {
            List<Transaction> transactionList = new List<Transaction>();
            User user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
            if (User.IsInRole("Mannager"))
            {
                transactionList = this.TransactionRepository.GetTransactionsByBankMananger(user.Id);
            }
            else
            {
                transactionList = this.TransactionRepository.GetTransactionsByUser(user.Id);
            }

            return View(transactionList);
        }

        [Authorize]
        // GET: Transactions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transaction transaction = this.TransactionRepository.Find(id.Value);
            if (transaction == null)
            {
                return HttpNotFound();
            }
            return View(transaction);
        }

        // GET: Transactions/Create
        [Authorize(Roles ="User")]
        public ActionResult Create()
        {
            FillViewBag();
            return View();
        }

        // POST: Transactions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "User")]
        public ActionResult Create([Bind(Include = "Amount,FromAccountId,ToAccountId")] Transaction transaction)
        {
            if (ModelState.IsValid)
            {
                if (transaction.FromAccountId != transaction.ToAccountId)
                {
                    BankAccount from = this.BankAccountRepository.Find(transaction.FromAccountId);
                    BankAccount to = this.BankAccountRepository.Find(transaction.ToAccountId);

                    if (from.Balance - transaction.Amount > from.Limit)
                    {
                        from.Balance -= transaction.Amount;
                        to.Balance += transaction.Amount;
                        this.BankAccountRepository.Update(from,true);
                        this.BankAccountRepository.Update(to, true);

                        this.TransactionRepository.Add(transaction, true);
                    }
                    else
                    {
                        ModelState.AddModelError("CustomErr", "Nemate dovoljno sredstava #DiSu");
                        FillViewBag();
                        return View(transaction);
                    }

                }else
                {
                    ModelState.AddModelError("CustomErr", "Računi ne smiju biti isti");
                    FillViewBag();
                    return View(transaction);
                }

                return RedirectToAction("Index");
            }

            FillViewBag();
            return View(transaction);
        }

        public void FillViewBag()
        {
            User user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
            ViewBag.FromAccountId = new SelectList(this.BankAccountRepository.FindByUserId(user.Id), "ID", "Description");
            ViewBag.ToAccountId = new SelectList(this.BankAccountRepository.GetList(), "ID", "Description");
        }
    }
}
