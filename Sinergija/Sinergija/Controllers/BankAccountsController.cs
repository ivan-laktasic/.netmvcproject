﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Sinergija.DAL;
using Sinergija.Model;
using Ninject;
using Sinergija.DAL.Repository;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Web.Security;

namespace Sinergija.Controllers
{
    public class BankAccountsController : Controller
    {
        [Inject]
        public BankAccountRepository BankAccountRepository { get; set; }
        [Inject]
        public BankRepository BankRepository { get; set; }

        private static Random random = new Random();




        // GET: BankAccounts
        [Authorize]
        public ActionResult Index()
        {
            List<BankAccount> bankAccList=new List<BankAccount>();
            string userID = System.Web.HttpContext.Current.User.Identity.GetUserId();
            if (User.IsInRole("Mannager"))
            {
                bankAccList = this.BankAccountRepository.FindAccountsByBankMannager(userID);
            }
            else
            {
                bankAccList = this.BankAccountRepository.FindByUserId(userID);
            }
     
            return View(bankAccList);
        }
      
        // GET: BankAccounts/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankAccount bankAccount = this.BankAccountRepository.Find(id.Value);
            bankAccount.User= System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(bankAccount.UserId);

            if (bankAccount == null)
            {
                return HttpNotFound();
            }
            return View(bankAccount);
        }

        // GET: BankAccounts/Create
        [Authorize(Roles="Mannager")]
        public ActionResult Create()
        {
            FillViewBag();
            return View();
        }

        // POST: BankAccounts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Mannager")]

        public ActionResult Create([Bind(Include = "BankAccountType,UserId,BankId,Balance,Limit,Description")] BankAccount bankAccount)
        {
         
            const string chars = "1234567890";
            string numericPart = new string(Enumerable.Repeat(chars, 19).Select(s => s[random.Next(s.Length)]).ToArray());
            bankAccount.IBAN="HR"+ numericPart;
            if (ModelState.IsValid)
            {
              
                this.BankAccountRepository.Add(bankAccount,true);
                
                return RedirectToAction("Index");
            }

            FillViewBag();
            return View(bankAccount);
        }

        // GET: BankAccounts/Edit/5
        [Authorize(Roles = "Mannager")]

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankAccount bankAccount = this.BankAccountRepository.Find(id.Value);
            if (bankAccount == null)
            {
                return HttpNotFound();
            }
            FillViewBag();
            return View(bankAccount);
        }

        // POST: BankAccounts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Mannager")]

        public ActionResult Edit(int id)
        {
            var model = this.BankAccountRepository.Find(id);
            var didUpdateModelSucceed = this.TryUpdateModel(model);

            if (didUpdateModelSucceed && ModelState.IsValid)
            {
                this.BankAccountRepository.Update(model, autoSave: true);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: BankAccounts/Delete/5
        [Authorize(Roles = "Mannager")]

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankAccount bankAccount = this.BankAccountRepository.Find(id.Value);
            if (bankAccount == null)
            {
                return HttpNotFound();
            }
            return View(bankAccount);
        }

        // POST: BankAccounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Mannager")]

        public ActionResult DeleteConfirmed(int id)
        {
            
            this.BankAccountRepository.Delete(id);
            
            return RedirectToAction("Index");
        }

        public void FillViewBag()
        {
            ApplicationUserManager userMannager = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            IQueryable<User> users = userMannager.Users;
            List<User> userList = new List<User>();
            foreach (User user in users.ToList())
            {
                if (userMannager.GetRoles(user.Id).Any(y => y == "User"))
                {
                    userList.Add(user);
                }
            }

            ViewBag.BankId = new SelectList(this.BankRepository.GetBanksByMannager(System.Web.HttpContext.Current.User.Identity.GetUserId()), "ID", "Name");
            ViewBag.UserId = new SelectList(userList, "Id", "Name");
        }
    }
}
