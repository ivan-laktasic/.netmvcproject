﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Sinergija.DAL;
using Sinergija.Model;
using Ninject;
using Sinergija.DAL.Repository;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace Sinergija.Controllers
{
    [Authorize(Roles ="Mannager")]
    [RoutePrefix("ovrhe")]
    public class EnforcementsController : Controller
    {
        [Inject]
        public EnforcmentRepository EnforcementRepository { get; set; }

        [Inject]
        public BankRepository BankRepository { get; set; }

        [Inject]
        public BankAccountRepository BankAccountRepository { get; set; }


        // GET: Enforcements
        public ActionResult Index()
        {
            var enforcements = EnforcementRepository.GetEforcementsByMannager(System.Web.HttpContext.Current.User.Identity.GetUserId());
            return View(enforcements.ToList());
        }

        // GET: Enforcements/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Enforcement enforcement = EnforcementRepository.Find(id.Value);
            if (enforcement == null)
            {
                return HttpNotFound();
            }
            return View(enforcement);
        }

        [Route("ovrsiNekoga")]
        // GET: Enforcements/Create
        public ActionResult Create()
        {
            FillViewBag();
            return View();
        }

        // POST: Enforcements/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Reason,UserId,BankId")] Enforcement enforcement)
        {
            if (ModelState.IsValid)
            {
                var bankAccList=BankAccountRepository.FindByUserId(enforcement.UserId);

                var bank = BankRepository.Find(enforcement.BankId);

                foreach(BankAccount acc in bankAccList)
                {
                    bank.Moneys += acc.Balance;
                    acc.Balance = 0;
                    BankAccountRepository.Update(acc);
                }
                BankRepository.Update(bank);

                EnforcementRepository.Add(enforcement,true);
                
                return RedirectToAction("Index");
            }

            FillViewBag();
            return View(enforcement);
        }



        public void FillViewBag()
        {
            List<User> userList = BankAccountRepository.FindAccountsByBankMannager(System.Web.HttpContext.Current.User.Identity.GetUserId()).Select(x => x.User).ToList();

            ViewBag.BankId = new SelectList(BankRepository.GetBanksByMannager(System.Web.HttpContext.Current.User.Identity.GetUserId()), "ID", "Name");
            ViewBag.UserId = new SelectList(userList, "Id", "Name");
        }
    }
}
