﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Sinergija.DAL;
using Sinergija.Model;
using Sinergija.DAL.Repository;
using Ninject;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;

namespace Sinergija.Controllers
{
    [Authorize(Roles ="Mannager")]
    public class BanksController : Controller
    {
        [Inject]
        public BankRepository BankRepositroy { get;set; }

        // GET: Banks
        public ActionResult Index()
        {
            return View(this.BankRepositroy.GetBanksByMannager(System.Web.HttpContext.Current.User.Identity.GetUserId()));
        }

        // GET: Banks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bank bank = this.BankRepositroy.Find(id.Value);
            if (bank == null)
            {
                return HttpNotFound();
            }
            return View(bank);
        }

        // GET: Banks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Banks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name,Moneys,DateCreated,DateModified")] Bank bank)
        {

            if (ModelState.IsValid)
            {
                var userMannager = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
                User user = userMannager.FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
                bank.UserId = user.Id;
                this.BankRepositroy.Add(bank,autoSave:true);

                return RedirectToAction("Index");
            }


            return View(bank);
        }

        // GET: Banks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bank bank = this.BankRepositroy.Find(id.Value);
            if (bank == null)
            {
                return HttpNotFound();
            }
            return View(bank);
        }

        // POST: Banks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id)
        {
            var model = this.BankRepositroy.Find(id);
            var didUpdateModelSucceed = this.TryUpdateModel(model);

            if (didUpdateModelSucceed && ModelState.IsValid)
            {
                this.BankRepositroy.Update(model, autoSave: true);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Banks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bank bank = this.BankRepositroy.Find(id.Value);
            if (bank == null)
            {
                return HttpNotFound();
            }
            return View(bank);
        }

        // POST: Banks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            this.BankRepositroy.Delete(id, true);
            return RedirectToAction("Index");
        }

    }
}
