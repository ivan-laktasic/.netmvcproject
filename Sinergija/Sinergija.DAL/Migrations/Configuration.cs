namespace Sinergija.DAL.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Sinergija.Model;
    using Microsoft.AspNet.Identity.EntityFramework;

    internal sealed class Configuration : DbMigrationsConfiguration<Sinergija.DAL.SinergijaDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Sinergija.DAL.SinergijaDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
           

            context.Roles.AddOrUpdate(
                p=>p.Id,
                new IdentityRole { Name="Mannager",Id="1"},
                new IdentityRole { Name = "User", Id = "2" }
                );

        }
    }
}
