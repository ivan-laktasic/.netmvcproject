namespace Sinergija.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Enorcements : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Enforcements",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Reason = c.String(nullable: false),
                        UserId = c.String(maxLength: 128),
                        BankId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Banks", t => t.BankId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.BankId);
            
            AddColumn("dbo.BankAccounts", "Description", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Enforcements", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Enforcements", "BankId", "dbo.Banks");
            DropIndex("dbo.Enforcements", new[] { "BankId" });
            DropIndex("dbo.Enforcements", new[] { "UserId" });
            DropColumn("dbo.BankAccounts", "Description");
            DropTable("dbo.Enforcements");
        }
    }
}
