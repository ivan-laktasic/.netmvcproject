﻿using Sinergija.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Sinergija.DAL
{
    public class SinergijaDBContext:IdentityDbContext<User>
    {
        public SinergijaDBContext():base("SinergijaDBContext", throwIfV1Schema: false)
        {

        }

        public static SinergijaDBContext Create()
        {
            return new SinergijaDBContext();
        }

        public DbSet<Bank> Banks { get; set; }
        public DbSet<BankAccount> BankAccounts { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Enforcement> Enforcements { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Transaction>()
                        .HasRequired(afc => afc.FromAccount)
                        .WithMany()
                        .HasForeignKey(afc => afc.FromAccountId)
                        .WillCascadeOnDelete(true);

            modelBuilder.Entity<Transaction>()
                        .HasRequired(afc => afc.ToAccount)
                        .WithMany()
                        .HasForeignKey(afc => afc.ToAccountId)
                        .WillCascadeOnDelete(false);
        }

    }
}
