﻿using Sinergija.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinergija.DAL.Repository
{
    public  class RepositoryBase<T>
        where T:EntityBase
    {
        protected SinergijaDBContext DbContext { get; }

        public RepositoryBase(SinergijaDBContext context)
        {
            this.DbContext = context;
        }

        public virtual List<T> GetList()
        {
            return this.DbContext.Set<T>()
                .ToList();
        }

        public virtual T Find(int id)
        {
            return this.DbContext.Set<T>()
                .Where(p => p.ID == id)
                .FirstOrDefault();
        }

        public void Save()
        {
            this.DbContext.SaveChanges();
        }

        public void Add(T model, bool autoSave = false)
        {
            model.DateCreated = DateTime.Now;

            this.DbContext.Set<T>().Add(model);

            if (autoSave)
                this.Save();
        }

        public void Update(T model, bool autoSave = false)
        {
            model.DateModified = DateTime.Now;

            this.DbContext.Entry(model).State = EntityState.Modified;

            if (autoSave)
                this.Save();
        }

        public virtual void Delete(int id, bool autoSave = false)
        {
            var entity = this.DbContext.Set<T>().Find(id);
            this.DbContext.Entry(entity).State = EntityState.Deleted;

            if (autoSave)
                this.Save();
        }
    }
}
