﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sinergija.Model;
using System.Data.Entity;

namespace Sinergija.DAL.Repository
{
    public class EnforcmentRepository:RepositoryBase<Enforcement>
    {
        public EnforcmentRepository(SinergijaDBContext context) : base(context) { }

        public List<Enforcement> GetEforcementsByMannager (string id)
        {
            return this.DbContext.Enforcements
                .Include(x => x.Bank)
                .Include(x => x.User)
                .Where(x => x.Bank.UserId == id)
                .ToList();
        }
                

    }
}
