﻿using Sinergija.Model;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinergija.DAL.Repository
{
    public class BankRepository : RepositoryBase<Bank>
    {
        public BankRepository(SinergijaDBContext context) : base(context) { }

        public override List<Bank> GetList()
        {
           return this.DbContext.Banks
                .Include(b => b.User)
                .ToList();
        }

        public List<Bank> GetBanksByMannager(string userId)
        {
            return this.DbContext.Banks
                .Include(x => x.User)
                .Where(x => x.UserId == userId)
                .ToList();
        }

        


        
    }
}
