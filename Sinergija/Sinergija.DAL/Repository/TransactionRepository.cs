﻿using Sinergija.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;


namespace Sinergija.DAL.Repository
{
    public  class TransactionRepository : RepositoryBase<Transaction>
    {
        public TransactionRepository(SinergijaDBContext context) : base(context) { }

        public List<Transaction> GetTransactionsByUser(string id)
        {
            return this.DbContext.Transactions
                .Include(x => x.FromAccount)
                .Where(x => x.FromAccount.UserId == id)
                .OrderBy(x => x.FromAccount.ID)
                .ToList();
                
        }

        public List<Transaction> GetTransactionsByBankMananger(string id)
        {
            return this.DbContext.Transactions
                .Include(x => x.FromAccount.Bank)
                .Include(x => x.ToAccount.Bank)
                .Where(x => x.FromAccount.Bank.UserId == id || x.ToAccount.Bank.UserId==id)
                .OrderBy(x=>x.FromAccount.ID)
                .ToList();

        }
    }
}
