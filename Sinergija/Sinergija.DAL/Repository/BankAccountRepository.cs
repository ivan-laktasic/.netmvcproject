﻿using Sinergija.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Sinergija.DAL.Repository
{
    public class BankAccountRepository:RepositoryBase<BankAccount>
    {
        public BankAccountRepository(SinergijaDBContext context) : base(context) { }

        public List<BankAccount> FindByUserId(String id)
        {
            return this.DbContext.BankAccounts
                .Include(p => p.Bank)
                .Include(p => p.User)
                .Where(p => p.UserId == id)
                .OrderBy(p => p.ID)
                .ToList();
        }

        public List<BankAccount> FindAccountsByBankMannager(String id)
        {
            return this.DbContext.BankAccounts
                .Include(p => p.Bank)
                .Include(p => p.User)
                .Where(p => p.Bank.UserId==id)
                .OrderBy(p => p.ID)
                .ToList();
        }

    }
}
