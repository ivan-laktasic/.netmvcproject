﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Sinergija.Model
{
    public class BankAccount:EntityBase
    {

        public BankAccountType BankAccountType { get; set; }

        [ForeignKey("User")]
        public string UserId { get; set; }

        public virtual User User { get; set; }

        [ForeignKey("Bank")]
        public int BankId { get; set; }

        public virtual Bank Bank { get; set; }

        [Required]
        [MinLength(7)]
        public string Description { get; set; }

        [Required]
        public double Balance { get; set; }

        [Required]
        public double Limit { get; set; }

        [StringLength(22)]
        public string IBAN { get; set; }

        public ICollection<Transaction> Transactions { get; set; }
    }

    public enum BankAccountType
    {
        Ziro,Tekuci
    }
}
