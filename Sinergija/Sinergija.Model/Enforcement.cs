﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinergija.Model
{
    public class Enforcement:EntityBase
    {
        [Required]
        [MinLength(15)]
        public string Reason { get; set; }

        [ForeignKey("User")]
        public string UserId { get; set; }

        public virtual User User { get; set; }


        [ForeignKey("Bank")]
        public int BankId { get; set; }

        public virtual Bank Bank { get; set; }
    }
}
