﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Sinergija.Model
{
    public class Bank:EntityBase
    {
        [Required]
        [StringLength(100,MinimumLength =2)]
        public string Name { get; set; }

        [ForeignKey("User")]
        public string UserId { get; set; }

        public User User { get; set; }

        public virtual ICollection<BankAccount> BankAccounts { get; set; }
        
        public double Moneys { get; set; }

        public ICollection<Enforcement> Enforcements { get; set; }

    }
}
