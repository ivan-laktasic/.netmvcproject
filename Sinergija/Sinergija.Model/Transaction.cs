﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;



namespace Sinergija.Model
{
    public class Transaction:EntityBase
    {
        public long Amount { get; set; }

       [ForeignKey("FromAccount")]
        public int FromAccountId { get; set; }
 
        public virtual BankAccount FromAccount { get; set; }

        [ForeignKey("ToAccount")]
        public int ToAccountId { get; set; }
      
        public virtual BankAccount ToAccount { get; set; }

       
    }
}
